package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	// "net/http"
	"net/url"
	"os"
	"os/user"
	"path/filepath"
	"time"
	
	// "golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/calendar/v3"	
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	r.GET("/get-token", func(c *gin.Context){

		b, err := ioutil.ReadFile("client_secret.json")
		if err != nil {
			log.Fatalf("Unable to read client secret file: %v", err)
		}		
		// If modifying these scopes, delete your previously saved credentials
		// at ~/.credentials/calendar-go-quickstart.json
		config, err := google.ConfigFromJSON(b, calendar.CalendarReadonlyScope)
		if err != nil {
			log.Fatalf("Unable to parse client secret file to config: %v", err)
		}
		token, err := getToken(c, config)

		c.JSON(200, gin.H{"token": token, "error": err})
	})

	r.GET("/get-auth-url", func(c *gin.Context){
		b, err := ioutil.ReadFile("client_secret.json")
        	if err != nil {                                
        	log.Fatalf("Unable to read client secret file: %v", err)
        }  
		// If modifying these scopes, delete your previously saved credentials
		// at ~/.credentials/calendar-go-quickstart.json
		config, err := google.ConfigFromJSON(b, calendar.CalendarReadonlyScope)
		if err != nil {
			log.Fatalf("Unable to parse client secret file to config: %v", err)
		}
		authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
		url :=  fmt.Sprintf("%v",authURL)
		c.JSON(200, map[string]string{"authURL": url})
	})

	r.POST("/save-token", func(c *gin.Context){

		cacheFile, err := tokenCacheFile()
		if err != nil {
			log.Fatalf("Unable to get path to cached credential file. %v", err)
		}		
		// Parse JSON
		var json struct {
			Token string `json:"token" binding:"required"`
		}

		if c.Bind(&json) == nil {
			b, err := ioutil.ReadFile("client_secret.json")
        		if err != nil {                                
        		log.Fatalf("Unable to read client secret file: %v", err)
        	}  
			config, err := google.ConfigFromJSON(b, calendar.CalendarReadonlyScope)
			if err != nil {
				log.Fatalf("Unable to parse client secret file to config: %v", err)
			}			
			tok, err := config.Exchange(oauth2.NoContext, json.Token)
			if err != nil {
				log.Fatalf("Unable to retrieve token from web %v", err)
			}

			saveToken(cacheFile, tok)
			c.JSON(200, gin.H{"status": "ok"})
		} else {
			c.JSON(200, gin.H{"status": "invalid token"})
		}
		
	})

	r.GET("/get-events", func(c *gin.Context){

		b, err := ioutil.ReadFile("client_secret.json")
		if err != nil {
			log.Fatalf("Unable to read client secret file: %v", err)
		}		
		// If modifying these scopes, delete your previously saved credentials
		// at ~/.credentials/calendar-go-quickstart.json
		config, err := google.ConfigFromJSON(b, calendar.CalendarReadonlyScope)
		if err != nil {
			log.Fatalf("Unable to parse client secret file to config: %v", err)
		}
		token, err := getToken(c, config)

		if err != nil {
			c.JSON(200, gin.H{"error": "Get and save your token first"})
			return
		}
		
		client := config.Client(c, token)
		srv, err := calendar.New(client)
		if err != nil {
			log.Fatalf("Unable to retrieve calendar Client %v", err)
		}

		t := time.Now().Format(time.RFC3339)
		events, err := srv.Events.List("primary").ShowDeleted(false).
			SingleEvents(true).TimeMin(t).MaxResults(10).OrderBy("startTime").Do()
		if err != nil {
			log.Fatalf("Unable to retrieve next ten of the user's events. %v", err)
		}

		c.JSON(200, gin.H{"events": events.Items})

			// if len(events.Items) > 0 {
			// 	for _, i := range events.Items {
			// 	var when string
			// 	// If the DateTime is an empty string the Event is an all-day Event.
			// 	// So only Date is available.
			// 	if i.Start.DateTime != "" {
			// 		when = i.Start.DateTime
			// 	} else {
			// 		when = i.Start.Date
			// 	}
			// 	fmt.Printf("%s (%s)\n", i.Summary, when)
			// 	}
			// } else {
			// 	fmt.Printf("No upcoming events found.\n")
			// }
	})

	// Listen and Server in 0.0.0.0:8080
	r.Run(":81")
}

func getToken(ctx *gin.Context, config *oauth2.Config) (*oauth2.Token, error) {
  cacheFile, err := tokenCacheFile()
  if err != nil {
    log.Fatalf("Unable to get path to cached credential file. %v", err)
  }
  tok, err := tokenFromFile(cacheFile)
  if err != nil {
    tok = nil
  }
  return tok, err
}

// tokenCacheFile generates credential file path/filename.
// It returns the generated credential path/filename.
func tokenCacheFile() (string, error) {
  usr, err := user.Current()
  if err != nil {
    return "", err
  }
  tokenCacheDir := filepath.Join(usr.HomeDir, ".credentials")
  os.MkdirAll(tokenCacheDir, 0700)
  return filepath.Join(tokenCacheDir,
    url.QueryEscape("calendar-go-quickstart.json")), err
}

// tokenFromFile retrieves a Token from a given file path.
// It returns the retrieved Token and any read error encountered.
func tokenFromFile(file string) (*oauth2.Token, error) {
  f, err := os.Open(file)
  if err != nil {
    return nil, err
  }
  t := &oauth2.Token{}
  err = json.NewDecoder(f).Decode(t)
  defer f.Close()
  return t, err
}

// saveToken uses a file path to create a file and store the
// token in it.
func saveToken(file string, token *oauth2.Token) {
  fmt.Printf("Saving credential file to: %s\n", file)
  f, err := os.Create(file)
  if err != nil {
    log.Fatalf("Unable to cache oauth token: %v", err)
  }
  defer f.Close()
  json.NewEncoder(f).Encode(token)
}
