FROM billyteves/alpine:3.5.0

MAINTAINER markgeronimo23@gmail.com

RUN apk add --no-cache --virtual --update \
  tzdata

ADD ./bin/svc-google-calendar /usr/local/bin/svc-google-calendar

RUN chmod +x /usr/local/bin/svc-google-calendar

EXPOSE 81

ENTRYPOINT ["svc-google-calendar"]
